import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from './database.module';
import { UserModule } from './apps/user/user.module';
import { MqttModule } from './apps/mqtt/mqtt.module';
import { ScaraModule } from './apps/scara/scara.module';
import { RouteMiddleware } from './route.middleware';

@Module({
  imports: [DatabaseModule, UserModule, MqttModule, ScaraModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(RouteMiddleware)
      .forRoutes({ path: '*', method: RequestMethod.ALL });
  }
}
