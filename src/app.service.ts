import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  constructor() {}
  // 说哈喽
  getHello(): string {
    return 'Hello World!';
  }
  // 获取接口公共数据结构
  getCommonResponse() {
    return {
      "code": 0,
      "data": {}
    }
  }
}
