// src/database/database.module.ts
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forRoot({
    type: 'sqlite',
    database: 'database.sqlite', // SQLite 数据库文件名
    entities: [__dirname + '/**/*.entity{.ts,.js}'],
    synchronize: true,
  })],
})
export class DatabaseModule {}