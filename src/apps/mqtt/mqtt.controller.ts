import { Controller, Get, Post, Delete, Patch, Param, Query, Body } from '@nestjs/common';
import { MqttService } from './mqtt.service';
import { getCommonResponse } from '../../utils';

@Controller('/mqtt')
export class MqttController {
  constructor(private readonly mqttService: MqttService) {}

  @Post('/publish')
  publish(@Body() body: any) {
    console.log('publish', body);
    this.mqttService.publish('/scara/z-arm-1832/99002', JSON.stringify(body));

    return getCommonResponse();
  }
}