import { Injectable } from '@nestjs/common';
import { Mqtt } from './mqtt';

@Injectable()
export class MqttService {
  private readonly mqtt: Mqtt = new Mqtt();

  constructor() {
    this.mockScaraMqttResponse();
  }
  // 模拟四轴机械臂mqtt回应
  mockScaraMqttResponse() {
    const jointsPID = {
      1: {
        0: { k_p: 900, k_i: 80 },
        1: { k_p: 900, k_i: 80 },
        2: { k_p: 7500, k_i: 5 },
        3: { k_p: 1800, k_i: 5 },
      },
      2: {
        0: { k_p: 900, k_i: 80 },
        1: { k_p: 900, k_i: 80 },
        2: { k_p: 7500, k_i: 5 },
        3: { k_p: 1800, k_i: 5 },
      },
      3: {
        0: { k_p: 900, k_i: 80 },
        1: { k_p: 900, k_i: 80 },
        2: { k_p: 7500, k_i: 5 },
        3: { k_p: 1800, k_i: 5 },
      },
      4: {
        0: { k_p: 900, k_i: 80 },
        1: { k_p: 900, k_i: 80 },
        2: { k_p: 7500, k_i: 5 },
        3: { k_p: 1800, k_i: 5 },
      }
    }
    
    const device_status = {
      '/scara/z-arm-1832/99001': {
        coord: `0,0,0,0,1`,
        joint: `0,0`,
        io_in_status: `000000000000`,
        io_out_status: `000000000000`,
        gripper: `20,0`,
        userCoordinate: [0,0,0,0],
        jogging: false,
        pid: JSON.parse(JSON.stringify(jointsPID))
      },
      '/scara/z-arm-1832/99002': {
        coord: `0,0,0,0,1`,
        joint: `0,0`,
        io_in_status: `000000000000`,
        io_out_status: `000000000000`,
        gripper: `20,0`,
        userCoordinate: [0,0,0,0],
        jogging: false,
        pid: JSON.parse(JSON.stringify(jointsPID))
      },
      '/scara/z-arm-1832/99003': {
        coord: `0,0,0,0,1`,
        joint: `0,0`,
        io_in_status: `000000000000`,
        io_out_status: `000000000000`,
        gripper: `20,0`,
        userCoordinate: [0,0,0,0],
        jogging: false,
        pid: JSON.parse(JSON.stringify(jointsPID))
      },
      '/scara/T1832C0/99001': {
        coord: `0,0,0,0,1`,
        joint: `0,0`,
        io_in_status: `000000000000`,
        io_out_status: `000000000000`,
        gripper: `20,0`,
        userCoordinate: [0,0,0,0],
        jogging: false,
        pid: JSON.parse(JSON.stringify(jointsPID))
      },
      '/scara/T1832C0/99002': {
        coord: `0,0,0,0,1`,
        joint: `0,0`,
        io_in_status: `000000000000`,
        io_out_status: `000000000000`,
        gripper: `20,0`,
        userCoordinate: [0,0,0,0],
        jogging: false,
        pid: JSON.parse(JSON.stringify(jointsPID))
      },
      '/scara/T2442C0/99001': {
        coord: `0,0,0,0,1`,
        joint: `0,0`,
        io_in_status: `000000000000`,
        io_out_status: `000000000000`,
        gripper: `20,0`,
        userCoordinate: [0,0,0,0],
        jogging: false,
        pid: JSON.parse(JSON.stringify(jointsPID))
      },
      '/scara/T2442C0/99002': {
        coord: `0,0,0,0,1`,
        joint: `0,0`,
        io_in_status: `000000000000`,
        io_out_status: `000000000000`,
        gripper: `20,0`,
        userCoordinate: [0,0,0,0],
        jogging: false,
        pid: JSON.parse(JSON.stringify(jointsPID))
      },
      '/sixaxis/FR3S622/99301': {
        baseDigitalOutStatus: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        endDigitalOutStatus: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        joints: [0,0,0,0,0,0],
        carts: [0,0,0,0,0,0],
        endOutAnalogQuantity: 0,
        motionStatus: 'RUNNING',
        mode: 1
      },
      '/sixaxis/FR3S622/99302': {
        baseDigitalOutStatus: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        endDigitalOutStatus: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        joints: [0,0,0,0,0,0],
        carts: [0,0,0,0,0,0],
        endOutAnalogQuantity: 0,
        motionStatus: 'RUNNING',
        mode: 1
      }
    };

    for(const topic in device_status) {
      if(topic.startsWith('/scara/')) {
        this.mqtt.subscribeTopicCallback({ id: topic, topic: topic, callback: (e)=> {
          try {
            const data = JSON.parse(e);
        
            if(data.MesType == 'Request') {
              data['MesType'] = 'Response';
              data['Get'] = {};
              data['Resource'] = '3D';
              data['Status'] = {
                "Work": "Done",
                "Alarms": null,
                "ErrorCode": null
              };
      
              if(data.Function == 'GetScaraParam') {
                data['Get'] = {
                  "coord": device_status[topic].coord,
                  "joint": device_status[topic].joint,
                  "connected": true,
                  "initial": true,
                  "gripper": device_status[topic].gripper,
                  "drag_teach": null,
                  "do": null,
                  "move_tag": false,
                  "communicate_tag": null,
                };
              }else if(data.Function == 'GetAllDigitalOut') {
                data['Get'] = {
                  "state": device_status[topic].io_out_status
                };
              }else if(data.Function == 'GetAllDigitalIn') {
                console.log('GetAllDigitalIn');
                device_status[topic].io_in_status = `${this.generateRandom1Or0()}${this.generateRandom1Or0()}${this.generateRandom1Or0()}${this.generateRandom1Or0()}${this.generateRandom1Or0()}${this.generateRandom1Or0()}${this.generateRandom1Or0()}${this.generateRandom1Or0()}${this.generateRandom1Or0()}${this.generateRandom1Or0()}${this.generateRandom1Or0()}${this.generateRandom1Or0()}`;
  
                data['Get'] = {
                  "state": device_status[topic].io_in_status
                };
              }else if(data.Function == 'SetDigitalOut') {
                const io_out_status = device_status[topic].io_out_status.split('');
                io_out_status[data['Set']['io_number']] = data['Set']['io_value'];
                device_status[topic].io_out_status = io_out_status.join('');
              }else if(['jog_movej2', 'jog_movel2'].includes(data.Function)) {
                const coords = device_status[topic].coord.split(',').map(item=> Number(item));
                const joints = device_status[topic].joint.split(',').map(item=> Number(item));
                const distance = Number(data['Set'].distance);
  
                if(data.Function == 'jog_movel2') {
                  if(data['Set'].axis == '1') {
                    coords[0] = coords[0] + distance;
                  }else if(data['Set'].axis == '2') {
                    coords[1] = coords[1] + distance;
                  }else if(data['Set'].axis == '3') {
                    coords[2] = coords[2] + distance;
                  }
                }else if(data.Function == 'jog_movej2') {
                  if(data['Set'].axis == '1') {
                    joints[0] = joints[0] + distance;
                  }else if(data['Set'].axis == '2') {
                    joints[1] = joints[1] + distance;
                  }else if(data['Set'].axis == '4') {
                    coords[3] = coords[3] + distance;
                  }
                }
  
                device_status[topic].joint = joints.join(',');
                device_status[topic].coord = coords.join(',');
              }else if(data.Function == 'MoveJ') {
                console.log(data);
              }else if(data.Function == 'MoveL') {
                console.log(data);
              }else if(data.Function == 'SetGripperStatus') {
                console.log(data);
                device_status[topic]['gripper'] = data.Set['gripper'];
              }else if(data.Function == 'JointHome') {
                console.log(e);
              }else if(data.Function == 'GetJointStateFront') {
                data['Get'] = {
                  "state": [this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),device_status[topic].jogging ? 6 : this.generateRandom1Or0()]
                };
              }else if(data.Function == 'GetToolCoordinate') {
                data['Get'] = {
                  "state": [this.generateRandomNumberWithDecimals(),this.generateRandomNumberWithDecimals()]
                };
              }else if(data.Function == 'GetUserCoordinate') {
                data['Get'] = {
                  "state": device_status[topic].userCoordinate
                };
              }else if(data.Function == 'SetUserCoordinate') {
                device_status[topic].userCoordinate = [data['Set']['x0'], data['Set']['x0'], data['Set']['x1'], data['Set']['y1']];
              }else if(data.Function == 'GetErrorReason') {
                data['Status'] = {
                  "Work": "Done",
                  "Alarms": null,
                  "ErrorCode": "30000"
                };
              }else if(data.Function == 'GetJointTorque') {
                data['Get'] = {
                  "state": [this.generateRandomNumberWithDecimals(),this.generateRandomNumberWithDecimals(),this.generateRandomNumberWithDecimals(),this.generateRandomNumberWithDecimals()]
                };
              }else if(data.Function == 'GetWorld') {
                data['Get'] = {
                  "state": [this.generateRandomNumberWithDecimals(),this.generateRandomNumberWithDecimals(),this.generateRandomNumberWithDecimals(),this.generateRandomNumberWithDecimals()]
                };
              }else if(data.Function == 'SetTeachMode') {
                device_status[topic].jogging = data['Set']['drag_teach'];
              }else if(data.Function == 'GetPid') {
                data['Get'] = {
                  "state": {
                    "k_p": device_status[topic].pid[data['Set']['joint_id']][data['Set']['pid_type']].k_p,
                    "k_i": device_status[topic].pid[data['Set']['joint_id']][data['Set']['pid_type']].k_i
                  }
                };
              }else if(data.Function == 'SetPid') {
                device_status[topic].pid[data['Set']['joint_id']][data['Set']['pid_type']].k_p = data['Set']['k_p'];
                device_status[topic].pid[data['Set']['joint_id']][data['Set']['pid_type']].k_i = data['Set']['k_i'];
              }
  
              setTimeout(()=> {
                this.mqtt.publish(topic, JSON.stringify(data));
              }, 500);
            }
          } catch (error) {
            console.log(error);
          }
        } });
      }else if(topic.startsWith('/sixaxis/')) {
        this.mqtt.subscribeTopicCallback({ id: topic, topic: topic, callback: (e)=> {
          try {
            const data = JSON.parse(e);
        
            if(data.MesType == 'Request') {
              data['MesType'] = 'Response';
              data['Get'] = {};
              data['Resource'] = '3D';
              data['Status'] = {
                "Work": "Done",
                "Alarms": null,
                "ErrorCode": null
              };

              if(data.Function == 'GetStatus') {
                data['Get'] = {
                  "joints": device_status[topic].joints,
                  "carts": device_status[topic].carts,
                  "mode": device_status[topic].mode,
                  "initial": true,
                  "endf_ai": this.generateRandomNumberWithDecimals(),
                  "endf_ao": device_status[topic].endOutAnalogQuantity,
                  "endf_di": [this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0()],
                  "endf_do": device_status[topic].endDigitalOutStatus,
                  "work_id": 11,
                  "tool_id": 12,
                  "base_do": device_status[topic].baseDigitalOutStatus,
                  "base_di": [this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0(),this.generateRandom1Or0()],
                  "motorstate": [0, 1, 2, 3, 4, 5],
                  "motion_status": device_status[topic].motionStatus,
                  "lr": 1,
                  "ud": 1,
                  "fn": 1
                };
              }else if(data.Function == 'GetJointTorque') {
                data['Get'] = {
                  "state": [this.generateRandomNumberWithDecimals(),this.generateRandomNumberWithDecimals(),this.generateRandomNumberWithDecimals(),this.generateRandomNumberWithDecimals(),this.generateRandomNumberWithDecimals(),this.generateRandomNumberWithDecimals()]
                };
              }else if(data.Function == 'SetBaseDO') {
                const baseDigitalOutStatus = device_status[topic].baseDigitalOutStatus;
                baseDigitalOutStatus[data['Set']['DOIndex']] = data['Set']['DOValue'];
                device_status[topic].baseDigitalOutStatus = baseDigitalOutStatus;
              }else if(data.Function == 'SetEndfDO') {
                const endDigitalOutStatus = device_status[topic].endDigitalOutStatus;
                endDigitalOutStatus[data['Set']['DOIndex']] = data['Set']['DOValue'];
                device_status[topic].endDigitalOutStatus = endDigitalOutStatus;
              }else if(data.Function == 'jog_movej2') {
                const joints = device_status[topic].joints;
                joints[Number(data['Set'].axis) - 1] += Number(data['Set'].distance);
                device_status[topic].joints = joints;
              }else if(data.Function == 'SetEndFAO') {
                device_status[topic].endOutAnalogQuantity = data['Set']['AOValue'];
              }else if(data.Function == 'JogKeydown') {
                if(data['Set']['coord_type'] == 0) {
                  device_status[topic].joints[Number(data['Set']['index'])] += Number(data['Set']['max_dist']) * Number(data['Set']['direction']);
                }else {
                  device_status[topic].carts[Number(data['Set']['index'])] += Number(data['Set']['max_dist']) * Number(data['Set']['direction']);
                }
              }else if(data.Function == 'GetDeviceError') {
                data['Status']['ErrorCode'] = [3,4];
              }else if(data.Function == 'Pause') {
                if(data['Set']['is_pause'] == 1) {
                  device_status[topic].motionStatus = 'PAUSING';
                }else if(data['Set']['is_pause'] == 0) {
                  device_status[topic].motionStatus = 'RUNNING';
                }
              }else if(data.Function == 'SetMode') {
                device_status[topic].mode = data['Set']['mode'];

                if(data['Set']['mode'] == 1) {
                  device_status[topic].motionStatus = 'DRAGGING';
                }else {
                  device_status[topic].motionStatus = 'PAUSING';
                }
              }

              setTimeout(()=> {
                this.mqtt.publish(topic, JSON.stringify(data));
              }, 500);
            }
          } catch (error) {
            console.log(error);
          }
        }});
      }
    }
  }

  // 随机生成0或1
  generateRandom1Or0() {
    return parseInt(String(Math.random()*10))%2;
  }

  // 生成 -100 到 500 之间的随机数（包括小数点后3位）
  generateRandomNumberWithDecimals() {
    const randomNumber = (Math.random() * (600) - 100).toFixed(3);
    return parseFloat(randomNumber); // 将字符串转换为浮点数
  }

  // 发布消息
  publish(topic, message, options = {}) {
    this.mqtt.publish(topic, message, options);
  }
}
