import * as mqtt from "mqtt";
import * as md5 from 'md5';

interface TopicObject { topic: string; id: string; callback: Function };

export class Mqtt {
  private publishDebouceDuration = 0;  // 防抖时长
  private mqttOptions =  { 
    clean: true, 
    clientId: `topology111`,
    keepalive: 60,
    reconnectPeriod: 3000,
    connectTimeout: 10000,
  };
  private publishOptions: mqtt.IClientPublishOptions = {
    qos: 0, 
    retain: false
  };
  private client: mqtt.MqttClient;
  private subscribedTopicCallbacks: TopicObject[] = [];
  private statusTimer;
  private heartbeatTimer;
  private unsubscribeTimer;
  private publishRecord: { [key: string]: number; } = {};
  private waitingToUnsubscribeTopics: { [key: TopicObject['topic']]: number } = {};

  constructor() {
    // 开始状态轮询
    this.statusPolling();
    // 进行心跳
    this.heartbeat();
    // 取消订阅主题轮询
    this.unsubscribePolling();
  }

  // 客户端状态轮询方法
  statusPolling() {
    // 客户端状态判断重连
    if(!this.client) {
      this.connect();
    }else {
      if(this.client.reconnecting) {
        console.log('正在重新连接...');
      }else if(!this.client.connected) {
        console.log('重新发起连接...');
        this.client.reconnect();
      }
    }

    // 清理已过期的发布记录
    for(let key in this.publishRecord) {
      if(this.publishRecord[key] < Date.now()) {
        delete this.publishRecord[key];
      }
    }

    clearTimeout(this.statusTimer);
    this.statusTimer = setTimeout(this.statusPolling.bind(this), 2000);
  }

  // 心跳
  heartbeat() {
    this.publish('heartbeat', 'heartbeat');
    clearTimeout(this.heartbeatTimer);
    this.heartbeatTimer = setTimeout(this.heartbeat.bind(this), 10000);
  }

  // 取消订阅轮询
  unsubscribePolling() {
    for(let topic in this.waitingToUnsubscribeTopics) {
      if(this.waitingToUnsubscribeTopics[topic] < Date.now()) {
        this.unsubscribe(topic).then(()=> {
          this.waitingToUnsubscribeTopics[topic] && delete this.waitingToUnsubscribeTopics[topic];
        });
      }
    }

    clearTimeout(this.unsubscribeTimer);
    this.unsubscribeTimer = setTimeout(this.unsubscribePolling.bind(this), 10000);
  }

  // 创建客户端方法
  connect() {
    this.client = mqtt.connect(`mqtt://192.168.1.61`, this.mqttOptions);
    
    // 监听客户端连接状态
    this.client.on("connect", () => {
      console.log('connected');

      this.client?.subscribe('heartbeat');
    });
    
    // 监听消息
    this.client.on("message", (topic, message) => {
      const subscribes = this.subscribedTopicCallbacks.filter(item=> item.topic == topic);
      const content = message.toString();

      // 给所有订阅该主题的订阅者反馈消息
      subscribes.forEach(item=> {
        item.callback(content);
      });
    });
    
    // 监听错误
    this.client.on("error", (e) => {
      console.log('error', e);
    });
    
    // 监听断开连接
    this.client.on('disconnect', ()=> {
      console.log('disconnected');
    });

    // 监听关闭连接
    this.client.on('close', ()=> {
      console.log('closed');
    });

    // 监听关闭连接
    this.client.on('end', ()=> {
      console.log('end');
    });

    // 监听关闭连接
    this.client.on('reconnect', ()=> {
      console.log('reconnect');
    });
  }

  // 订阅主题回调
  subscribeTopicCallback(topicObject: TopicObject) {
    return new Promise((resolve, reject)=> {
      if(this.client) {
        const existed = this.subscribedTopicCallbacks.some(item=> item.id == topicObject.id);

        if(existed) {
          reject('重复订阅！');
        }else {
          if(this.waitingToUnsubscribeTopics[topicObject.topic]) {  // 如果该主题还处于等待取消订阅中，则移出取消订阅队列
            delete this.waitingToUnsubscribeTopics[topicObject.topic];
          }else if(!this.subscribedTopicCallbacks.some(item=> item.topic == topicObject.topic)) {  // 如果没有订阅，则发起主题订阅
            this.subscribe(topicObject.topic);
          }
    
          this.subscribedTopicCallbacks.push(topicObject);
          resolve('已订阅回调！');
        }
      }else {
        reject('客户端未创建！');
      }
    });
  }

  // 取消订阅主题回调
  unsubscribeTopicCallback(topicId: TopicObject['id']) {
    return new Promise((resolve, reject)=> {
      if(this.client) {
        const topicObject = this.subscribedTopicCallbacks.find(item=> item.id == topicId);

        if(topicObject) {
          const newSubscribedTopics = this.subscribedTopicCallbacks.filter(item=> item.topic == topicObject.topic && item.id != topicObject.id);

          if(newSubscribedTopics.length == 0) {  // 如果全部主题回调被取消订阅，则取消主题的订阅
            this.waitingToUnsubscribeTopics[topicObject.topic] = Date.now() + 10000;  // 10秒后再取消订阅
          }
          this.subscribedTopicCallbacks = newSubscribedTopics;
          resolve('取消订阅当前主题回调');
        }else {
          resolve('未订阅该主题回调');
        }
      }else {
        reject('客户端未创建！');
      }
    });
  }

  // 订阅主题
  subscribe(topic: TopicObject['topic']) {
    console.log('订阅主题', topic);
    return new Promise((resolve, reject)=> {
      if(this.client) {
        this.client.subscribe(topic, (err) => {
          if (err) {
            reject(err);
          }else {
            resolve('订阅成功！');
          }
        });
      }else {
        reject();
      }
    });
  }

  // 取消订阅主题
  unsubscribe(topic: TopicObject['topic']) {
    console.log('取消订阅', topic);
    return new Promise((resolve, reject)=> {
      if(this.client) {
        this.client.unsubscribe(topic, {}, (err)=> {
          if (err) {
            reject(err);
          }else {
            resolve('取消订阅成功！');
          }
        });
      }else {
        reject();
      }
    });
  }

  // 发布消息
  publish(topic: string, message: any, configs?: { expire?: number; }) {
    return new Promise((resolve, reject)=> {
      if(this.client) {
        const expire = configs?.expire || this.publishDebouceDuration;
        const key = md5(configs?.expire ? `${topic}` : `${topic}${message}`);
        const now = Date.now();

        if(this.publishRecord[key] && now < (this.publishRecord[key] || 0)) {
          resolve(true);
        }else {
          this.publishRecord[key] = now + expire;
          this.client.publish(topic, message, this.publishOptions, (err)=> {
            if (err) {
              reject(err);
            }else {
              resolve(true);
            }
          });
        }
      }else {
        reject('客户端未创建！');
      }
    });
  }
}
