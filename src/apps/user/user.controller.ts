import { Controller, Get, Post, Delete, Patch, Put, Param, Query, Body } from '@nestjs/common';
import { UserService } from './user.service';
import { getCommonResponse } from 'src/utils';

@Controller('/user')
export class UserController {
  constructor(readonly userService: UserService) {}

  @Get('/:id')
  async findRow(@Param() param: any) {
    const data = getCommonResponse();
    data.data = await this.userService.findOne(param.id);

    return data;
  }

  @Get()
  async findRows(@Query() query: any) {
    const data = getCommonResponse();
    data.data = await this.userService.findAll();

    return data;
  }

  @Put()
  async createRow(@Body() body: any) {
    const data = getCommonResponse();
    delete body.id;
    data.data = await this.userService.createUser(body);

    return data;
  }
}