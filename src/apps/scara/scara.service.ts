import { Injectable } from '@nestjs/common';
import { Coordination, Trajectory } from './scara.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like, RegExpOrString } from 'typeorm';

@Injectable()
export class ScaraCoordinationService {
  constructor(
    @InjectRepository(Coordination)
    private readonly coordinationRepository: Repository<Coordination>,
  ) {}

  async findAll(options): Promise<{ data: Coordination[], total: number, page: number, size: number }> {
    const page = Number(options.page || 1);
    const pageSize = Number(options.size || 10);

    const [data, total] = await this.coordinationRepository
      .createQueryBuilder('coordination')
      .skip((page - 1) * pageSize)
      .take(pageSize)
      .getManyAndCount();

      // console.log(data);

    return { data, total, page, size: pageSize };
  }

  async findOne(id: number): Promise<Coordination> {
    return this.coordinationRepository.findOneById(id);
  }

  async create(data: Coordination): Promise<Coordination> {
    const createTime = data.create_at || Date.now();

    return this.coordinationRepository.save({ ...data, create_at: createTime, update_at: createTime });
  }

  async update(id, data: Coordination): Promise<any> {
    return this.coordinationRepository.update(id, { ...data, update_at: Date.now() });
  }

  async delete(id): Promise<any> {
    return this.coordinationRepository.delete(id);
  }
}

@Injectable()
export class ScaraTrajectoryService {
  constructor(
    @InjectRepository(Trajectory)
    private readonly trajectoryRepository: Repository<Trajectory>,
  ) {}

  async findAll(options): Promise<{ data: Trajectory[], total: number, page: number, size: number }> {
    const page = Number(options.page || 1);
    const pageSize = Number(options.size || 10);

    const [data, total] = await this.trajectoryRepository
      .createQueryBuilder('trajectory')
      .skip((page - 1) * pageSize)
      .take(pageSize)
      .getManyAndCount();

      // console.log(data);

    return { data, total, page, size: pageSize };
  }

  async findOne(id: number): Promise<Trajectory> {
    return this.trajectoryRepository.findOneById(id);
  }

  async create(data: Trajectory): Promise<Trajectory> {
    const createTime = data.create_at || Date.now();

    return this.trajectoryRepository.save({ ...data, create_at: createTime, update_at: createTime });
  }

  async update(id, data: Trajectory): Promise<any> {
    return this.trajectoryRepository.update(id, { ...data, update_at: Date.now() });
  }

  async delete(id): Promise<any> {
    return this.trajectoryRepository.delete(id);
  }
}