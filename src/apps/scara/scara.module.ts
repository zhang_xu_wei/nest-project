import { Module } from '@nestjs/common';
import { ScaraCoordinationService, ScaraTrajectoryService } from './scara.service';
import { ScaraCoordinationController, ScaraTrajectoryController } from './scara.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Coordination, Trajectory } from './scara.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Coordination, Trajectory])],
  controllers: [ ScaraCoordinationController, ScaraTrajectoryController],
  providers: [ScaraCoordinationService, ScaraTrajectoryService]
})
export class ScaraModule {}
