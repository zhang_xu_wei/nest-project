import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class Coordination {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  co_name: string;

  @Column()
  device_id: string;

  @Column({ type: 'float' })
  c_x: number;

  @Column({ type: 'float' })
  c_y: number;

  @Column({ type: 'float' })
  c_z: number;

  @Column({ type: 'float' })
  c_r: number;

  @Column({ type: 'float' })
  c_angle1: number;

  @Column({ type: 'float' })
  c_angle2: number;

  @Column({ type: 'float' })
  c_j1: number;

  @Column({ type: 'float' })
  c_j2: number;

  @Column({ type: 'float' })
  c_j3: number;

  @Column({ default: null })
  create_at: number;

  @Column({ default: null })
  update_at: number;
}

@Entity()
export class Trajectory {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  tk_name: string;

  @Column()
  device_id: string;

  @Column({ type: 'json' })
  points: any;

  @Column({ default: null })
  create_at: number;

  @Column({ default: null })
  update_at: number;
}