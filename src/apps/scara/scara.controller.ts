import { Controller, Get, Post, Delete, Patch, Param, Query, Body } from '@nestjs/common';
import { ScaraCoordinationService, ScaraTrajectoryService } from './scara.service';
import { getCommonResponse } from 'src/utils';

@Controller('/backend/coordination/scara')
export class ScaraCoordinationController {
  constructor(private readonly scaraCoordinationService: ScaraCoordinationService) {}

  @Get()
  async getCoordinations(@Query() query: any) {
    const data = getCommonResponse();
    const result = await this.scaraCoordinationService.findAll(query);

    data.data = result;

    return data;
  }

  @Post()
  async addCoordination(@Body() body: any) {
    const data = getCommonResponse();
    delete body.id;
    data.data = await this.scaraCoordinationService.create(body);

    return data;
  }

  @Patch(':id')
  async updateCoordination(@Param() param: any, @Body() body: any) {
    const data = getCommonResponse();
    data.data = await this.scaraCoordinationService.update(param.id, body);

    return data;
  }

  @Delete(':id')
  async deleteCoordination(@Param() param: any) {
    const data = getCommonResponse();
    data.data = await this.scaraCoordinationService.delete(param.id);

    return data;
  }
}

@Controller('/backend/track/scara')
export class ScaraTrajectoryController {
  constructor(private readonly scaraTrajectoryService: ScaraTrajectoryService) {}

  @Get()
  async getTrajectorys(@Query() query: any) {
    const data = getCommonResponse();
    const result = await this.scaraTrajectoryService.findAll(query);

    data.data = result;

    return data;
  }

  @Post()
  async addTrajectory(@Body() body: any) {
    const data = getCommonResponse();
    delete body.id;
    data.data = await this.scaraTrajectoryService.create(body);

    return data;
  }

  @Patch(':id')
  async updateTrajectory(@Param() param: any, @Body() body: any) {
    const data = getCommonResponse();
    data.data = await this.scaraTrajectoryService.update(param.id, body);

    return data;
  }

  @Delete(':id')
  async deleteTrajectory(@Param() param: any) {
    const data = getCommonResponse();
    data.data = await this.scaraTrajectoryService.delete(param.id);

    return data;
  }
}